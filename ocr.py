# Pemakaian
# python ocr.py --image images/example_01.png
# python ocr.py --image images/example_02.png  --preprocess blur

import imutils as imutils
from PIL import Image, ImageFilter
from pytesseract import Output
from pyimagesearch.transform import four_point_transform
from skimage.filters import threshold_local
from scipy.ndimage import interpolation as inter
from skimage import exposure
from scipy import ndimage
import pytesseract
import argparse
import numpy as np
import cv2
import csv
import os
import math
import requests

pytesseract.pytesseract.tesseract_cmd = r"/Users/jasonhalim/mypython/lib/python3.8/site-packages/tesseract/tesseract"

# pytesseract.pytesseract.tesseract_cmd = r"/usr/local/Cellar/tesseract/4.1.1/bin/tesseract"

# constructor buat argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="path to input image to be OCR'd")
ap.add_argument("-p", "--preprocess", type=str, default="thresh",
                help="type of preprocessing to be done")
args = vars(ap.parse_args())


def set_image_dpi(file_path):
    im = Image.open(file_path)
    length_x, width_y = im.size
    factor = min(1, float(1024.0 / length_x))
    size = int(factor * length_x), int(factor * width_y)
    im_resized = im.resize(size, Image.ANTIALIAS)
    filename = "{}.png".format(os.getpid())
    im_resized.save(filename, dpi=(300, 300))
    return filename


imagedpi = set_image_dpi(args["image"])
bukangambar = False
image = cv2.imread(imagedpi)
orig = image.copy()


def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")

    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]

    # return the ordered coordinates
    return rect


# function to transform image to four points
def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)

    # # multiply the rectangle by the original ratio
    # rect *= ratio

    (tl, tr, br, bl) = rect

    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # return the warped image
    return warped


# function to find two largest countours which ones are may be
#  full image and our rectangle edged object
def findLargestCountours(cntList, cntWidths):
    newCntList = []
    newCntWidths = []

    # finding 1st largest rectangle
    first_largest_cnt_pos = cntWidths.index(max(cntWidths))

    # adding it in new
    newCntList.append(cntList[first_largest_cnt_pos])
    newCntWidths.append(cntWidths[first_largest_cnt_pos])

    # removing it from old
    cntList.pop(first_largest_cnt_pos)
    cntWidths.pop(first_largest_cnt_pos)

    # finding second largest rectangle
    seccond_largest_cnt_pos = cntWidths.index(max(cntWidths))

    # adding it in new
    newCntList.append(cntList[seccond_largest_cnt_pos])
    newCntWidths.append(cntWidths[seccond_largest_cnt_pos])

    # removing it from old
    cntList.pop(seccond_largest_cnt_pos)
    cntWidths.pop(seccond_largest_cnt_pos)

    return newCntList, newCntWidths


def convert_object(image, screen_size=None, isDebug=False):
    # image = imutils.resize(image, height=300)
    # ratio = image.shape[0] / 300.0

    # convert the image to grayscale, blur it, and find edges
    # in the image
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.bilateralFilter(gray, 11, 17, 17)  # 11  //TODO 11 FRO OFFLINE MAY NEED TO TUNE TO 5 FOR ONLINE

    gray = cv2.medianBlur(gray, 5)
    edged = cv2.Canny(gray, 30, 400)

    # find contours in the edged image, keep only the largest
    # ones, and initialize our screen contour

    countours, hierarcy = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    imageCopy = image.copy()

    # approximate the contour
    cnts = sorted(countours, key=cv2.contourArea, reverse=True)
    screenCntList = []
    scrWidths = []
    for cnt in cnts:
        peri = cv2.arcLength(cnt, True)  # cnts[1] always rectangle O.o
        approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
        screenCnt = approx
        # print(len(approx))

        if (len(screenCnt) == 4):
            (X, Y, W, H) = cv2.boundingRect(cnt)
            # print('X Y W H', (X, Y, W, H))
            screenCntList.append(screenCnt)
            scrWidths.append(W)

        # else:
        #     print("4 points not found")

    screenCntList, scrWidths = findLargestCountours(screenCntList, scrWidths)

    if not len(screenCntList) >= 2:  # there is no rectangle found
        return None
    elif scrWidths[0] != scrWidths[1]:  # mismatch in rect
        return None

    # now that we have our screen contour, we need to determine
    # the top-left, top-right, bottom-right, and bottom-left
    # points so that we can later warp the image -- we'll start
    # by reshaping our contour to be our finals and initializing
    # our output rectangle in top-left, top-right, bottom-right,
    # and bottom-left order
    pts = screenCntList[0].reshape(4, 2)

    rect = order_points(pts)

    # apply the four point tranform to obtain a "birds eye view" of
    # the image
    warped = four_point_transform(image, pts)

    # convert the warped image to grayscale and then adjust
    # the intensity of the pixels to have minimum and maximum
    # values of 0 and 255, respectively
    warp = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
    warp = exposure.rescale_intensity(warp, out_range=(0, 255))

    # show the original and warped images

    if (screen_size != None):
        return cv2.cvtColor(cv2.resize(warp, screen_size), cv2.COLOR_GRAY2RGB)
    else:
        return cv2.cvtColor(warp, cv2.COLOR_GRAY2RGB)


# # driver function which identify 4 corners and doing four point transformation
# def convert_object(image):
#
#     # image = imutils.resize(image, height=300)
#     # ratio = image.shape[0] / 300.0
#
#     # convert the image to grayscale, blur it, and find edges
#     # in the image
#     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     gray = cv2.bilateralFilter(gray, 11, 17, 17)  # 11
#
#     gray = cv2.medianBlur(gray, 5)
#     edged = cv2.Canny(gray, 30, 400)
#
#
#     # find contours in the edged image, keep only the largest
#     # ones, and initialize our screen contour
#
#     countours, hierarcy = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
#
#
#     imageCopy = image.copy()
#
#     # approximate the contour
#     cnts = sorted(countours, key=cv2.contourArea, reverse=True)
#     screenCntList = []
#     scrWidths = []
#     for cnt in cnts:
#         peri = cv2.arcLength(cnt, True)  # cnts[1] always rectangle
#         approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
#         screenCnt = approx
#         # print(len(approx))
#
#         if (len(screenCnt) == 4):
#             (X, Y, W, H) = cv2.boundingRect(cnt)
#             # print('X Y W H', (X, Y, W, H))
#             screenCntList.append(screenCnt)
#             scrWidths.append(W)
#
#         # else:
#         #     print("4 points not found")
#
#     screenCntList, scrWidths = findLargestCountours(screenCntList, scrWidths)
#
#     if not len(screenCntList) >= 2:  # there is no rectangle found
#         return None
#     elif scrWidths[0] != scrWidths[1]:  # mismatch in rect
#         return None
#
#
#     # now that we have our screen contour, we need to determine
#     # the top-left, top-right, bottom-right, and bottom-left
#     # points so that we can later warp the image -- we'll start
#     # by reshaping our contour to be our finals and initializing
#     # our output rectangle in top-left, top-right, bottom-right,
#     # and bottom-left order
#     pts = screenCntList[0].reshape(4, 2)
#
#     rect = order_points(pts)
#
#     # apply the four point tranform to obtain a "birds eye view" of
#     # the image
#     warped = four_point_transform(image, pts)
#
#     # convert the warped image to grayscale and then adjust
#     # the intensity of the pixels to have minimum and maximum
#     # values of 0 and 255, respectively
#     warp = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
#     warp = exposure.rescale_intensity(warp, out_range=(0, 255))
#
#     # if (screen_size != None):
#     #
#     # else:
#     return cv2.cvtColor(cv2.resize(warp, screen_size), cv2.COLOR_GRAY2RGB)
try:
    warped = convert_object(orig)
except:
    bukangambar = True

# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# gray = cv2.GaussianBlur(gray, (5, 5), 0)
# edged = cv2.Canny(gray, 75, 200)
# # Mencari contour terbesar dari gambar, mengambil dan meninisialisasi
# cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
# cnts = imutils.grab_contours(cnts)
# cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
# # Looping untuk mencari contour
# for c in cnts:
#     # approximation contour
#     peri = cv2.arcLength(c, True)
#     approx = cv2.approxPolyDP(c, 0.02 * peri, True)
#     # jika contour ada 4 yaitu ujung kelancipan document
#     # tsb dari gambar maka kita menemukan gambar yang valid
#     if len(approx) == 4:
#         screenCnt = approx
#         bukangambar = 'cntr'
#         break
#     else:
#         bukangambar = 'nocntr'
#         break

# Menampilkan outline dari document
if bukangambar:
    def correct_skew(orig, delta=1, limit=5):
        def determine_score(arr, angle):
            data = inter.rotate(arr, angle, reshape=False, order=0)
            histogram = np.sum(data, axis=1)
            score = np.sum((histogram[1:] - histogram[:-1]) ** 2)
            return histogram, score

        gray = cv2.cvtColor(orig, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

        scores = []
        angles = np.arange(-limit, limit + delta, delta)
        for angle in angles:
            histogram, score = determine_score(thresh, angle)
            scores.append(score)

        best_angle = angles[scores.index(max(scores))]

        (h, w) = orig.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, best_angle, 1.0)
        rotated = cv2.warpAffine(orig, M, (w, h), flags=cv2.INTER_CUBIC, \
                                 borderMode=cv2.BORDER_REPLICATE)

        return best_angle, rotated


    angle, rotated = correct_skew(orig)
    kernel = np.ones((5, 5), np.uint8)
    # warped = cv2.erode(warped, kernel, iterations=1)
    # warped = cv2.dilate(warped, kernel, iterations=1)
# else:


# warped = four_point_transform(orig, screenCnt.reshape(4, 2) * ratio)


# Memproses gambar dengan grayscaling, thresholding untuk memberikan efek hitam putih

    warped = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)
# # warped= cv2.threshold(warped, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
# T = threshold_local(warped, 11, offset=10, method="gaussian")
# warped = (warped > T).astype("uint8") * 200
# kernel = np.ones((5, 5), np.uint8)
# warped = cv2.erode(warped, kernel, iterations=1)
# warped = cv2.dilate(warped, kernel, iterations=1)

# _, blackAndWhite = cv2.threshold(warped, 127, 255, cv2.THRESH_BINARY_INV)
#
# nlabels, labels, stats, centroids = cv2.connectedComponentsWithStats(blackAndWhite, None, None, None, 8, cv2.CV_32S)
# sizes = stats[1:, -1]  # get CC_STAT_AREA component
# img2 = np.zeros((labels.shape), np.uint8)
#
# for i in range(0, nlabels - 1):
#     if sizes[i] >= 8:  # filter small dotted regions
#         img2[labels == i + 1] = 255
#
# warped = cv2.bitwise_not(img2)


filename = "{}.png".format(os.getpid())
cv2.imwrite(filename, warped)
custom_config = r'--psm 3 -l tessdata/ind+eng+tessdata/ocrb'
text = pytesseract.image_to_data(Image.open(filename), output_type=Output.DICT, config=custom_config)
keys = list(text.keys())
os.remove(filename)

# boxes = pytesseract.image_to_data(warped, output_type=Output.DICT, config=custom_config)
#
# total_boxes = len(boxes['text'])
# for sequence_number in range(total_boxes):
#     if int(boxes['conf'][sequence_number]) >30:
#         (x, y, w, h) = (boxes['left'][sequence_number], boxes['top'][sequence_number], boxes['width'][sequence_number],  boxes['height'][sequence_number])
#         warped = cv2.rectangle(warped, (x, y), (x + w, y + h), (0, 255, 0), 2)
#
#
#
# parse_text = []
# word_list = []
# last_word = ''
# for word in boxes['text']:
#     if word!='':
#         word_list.append(word)
#         last_word = word
#     if (last_word!='' and word == '') or (word==boxes['text'][-1]):
#          parse_text.append(word_list)
#          word_list = []
#
#
#
#
# print(parse_text)


words = ""
boxes = pytesseract.image_to_data(warped, config=custom_config)
for a, b in enumerate(boxes.splitlines()):
    # print(b)
    if a != 0:
        b = b.split()
        if len(b) == 12:
            x, y, w, h = int(b[5]), int(b[6]), int(b[7]), int(b[8])
            # cv2.putText(image, b[11], (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (50, 50, 255), 2)
            # cv2.rectangle(warped, (x, y), (x + w, y + h), (50, 50, 255), 2)
            print(b[11])
            # words += "%s " % b[11]

cv2.imwrite('warped.png', imutils.resize(warped, height=650))

#                                  'Dispatched', 'Date', 'To', 'Transaction'
#                                  , 'Type', 'RMS', 'RTV', 'No', 'Max'
#                                  , 'Pickup', 'Data', 'KIP', 'Description'
#                                  , 'Qty', 'Reason', 'Code'])


#
# printedDate = "21 Jul 2020"
# from_ = "612 - PRIMO MAXX BOX KWC"
# to = "300000461 - ENSEVAL PUTERA M"
# address = "JL. PULO LENTUT NO.10 KIP - PULO GADUNG JAKARTA TIMUR JAKARTA"
# dispatchedDate = "12 JUL 2020"
# status = "DISPATCHED"
# transactionType = "Return to Vendor"
# rmsRtvNo = "3753153"
# simRtvNo = "45535542"
# returnAuthNo = "612_09891"
# maxPickupDate = "19 JUL 2020"
# no[]
# sku[]
# skuDescription[]
# qtyRequest[]
# qtyShip[]
# reasonCode[]
#
# response = requests.post('https://httpbin.org/post',
#     json={' ': printedDate, 'from': from_, 'to':to, 'address':address, 'dispatchedDate':dispatchedDate,
#     'status':status, 'transactionType':transactionType, 'rmsRtvNo':rmsRtvNo, 'simRtvNo':simRtvNo, 'returnAuthNo':returnAuthNo,
#     'maxPickupDate':maxPickupDate})
# print("------------------------")
# print("Status code: ", response.status_code)
# print("Printing Entire Post Request")
# print(response.json())


cv2.imshow("Original", orig)
cv2.imshow("Scanned", warped)
cv2.waitKey(0)
cv2.destroyAllWindows()
